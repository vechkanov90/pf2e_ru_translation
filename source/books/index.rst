
.. _Books:

=========================================================================================
Книги
=========================================================================================

.. toctree::
   :glob:
   :maxdepth: 2
   
   Secrets-of-Magic/index
   Guns-and-Gears/index
   Book-of-the-Dead/index
   Dark-Archive/index
   Rage-of-Elements/index
   LO-Impossible-Lands/index
   Kingmaker/index
   
   */index
