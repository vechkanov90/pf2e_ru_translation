
.. _Rage-of-Elements:

Ярость стихий (Rage of Elements)
============================================================================================================

**КЛАСС**

* :doc:`/classes/Kineticist`

**АРХЕТИПЫ**

* :doc:`/classes/archetypes/Kineticist`


.. toctree::
   :maxdepth: 2

   Elemental-Allies
