.. include:: /helpers/roles.rst


.. rst-class:: creature-details

Бодак (Bodak)
============================================================================================================

.. sidebar:: |rules|

	.. rubric:: Создание бодака (Bodak Creation)
	
	**Источник**: Bestiary 2 pg. 42
	
	Редкость событий, создающих бодаков, гарантирует, что большинство этих мерзостей были :t_humanoid:`гуманоидами`, убитыми взглядом другого бодака.
	Однако бодаки могут появиться и в результате :r_rare:`редкой` версии ритуала :ref:`ritual--Create-Undead`.
	Этот ужасный ритуал имитирует столкновение с абсолютным, сверхъестественным злом, поэтому заклинание должно начаться, когда субъект жив и находится на одном из злых :ref:`Внешних Планов <Planes--Outer-Sphere>`.


Когда живой, разумный :t_humanoid:`гуманоид` подвергается экстремальному проявлению сверхъестественного зла, этот опыт может безвозвратно проклясть жертву, сокрушая ее разум и вырывая душу в ужасающей, нечестивой трансформации, в результате которой появляется существо, являющееся анафемой жизни - бодак.

Части физического тела, уцелевшие после абсолютной порчи, служат лишь для того, чтобы подчеркнуть пугающую внешность гуманоида.
Тело бодака ужасающе скручено, как будто оно заперто в конвульсиях агонии и террора.
Его иссохшая и безволосая плоть, имеющая потусторонний перламутровый блеск, натянута на деформированный скелет, который сводит его походку к медленному шатанию.
Но самое страшное - это его глаза, расположенные на поникшем, оплавленном лице со скудными остатками прежней формы лица.
Эти глаза, глубоко запавшие в глазницы, тем не менее светятся нечестивым светом, смотрят с неослабевающей злобой и постоянно источают ядовитые испарения.

Обрывочные воспоминания о прежнем существовании, просеянные через мстительную ненависть к живым, заставляют бодака пытаться вернуться в те места, которые он когда-то знал.
Если это удается, он нападает на бывших друзей, знакомых и возлюбленных с помощью своего убийственного взгляда и непонятной тарабарщиной, изобилующей мерзкими проклятиями, обвинениями и угрозами - нападение, которое часто приводит к тому, что жертвы сами восстают в виде новых бодаков.



.. rst-class:: creature
.. _bestiary--Bodak:

Бодак (`Bodak <https://2e.aonprd.com/Monsters.aspx?ID=573>`_) / Существо 8
------------------------------------------------------------------------------------------------------------

- :uncommon:`необычный`
- :alignment:`ХЗ`
- :size:`средний`
- нежить

**Источник**: Bestiary 2 pg. 42

**Восприятие**: +17;
:ref:`ночное зрение <cr_ability--Darkvision>`,
:ref:`жизнечувствительность <cr_ability--Lifesense>` 60 футов

**Языки**: Бездны,
Всеобщий

**Навыки**:
Атлетика +15,
Акробатика +18,
Запугивание +19,
Скрытность +18

**Сил** +3,
**Лвк** +4,
**Тел** +1,
**Инт** -2,
**Мдр** +5,
**Хар** +5

----------

**КБ**: 27;
**Стойкость**: +13,
**Рефлекс**: +16,
**Воля**: +19

**ОЗ**: 160,
:ref:`негативное исцеление <cr_ability--Negative-Healing>`

**Иммунитеты**:
эффекты :t_death:`смерти`,
:t_disease:`болезнь`,
:t_poison:`яд`,
:c_paralyzed:`парализован`,
:c_unconscious:`без сознания`

**Слабости**: добро 10


**Уязвимость к солнечному свету (Sunlight Vulnerability)**: Если бодак подвергается прямому солнечному свету, то он не может использовать действия с признаком :t_death:`смерть` и становится :c_slowed:`замедлен 1`.
Величина :c_slowed:`замедления` увеличивается на 1 каждый раз, когда бодак заканчивает свой ход на солнечном свете.
Если бодак теряет все свои действия таким образом, то он уничтожается.


**Истощающий взгляд (Draining Glance)** |д-р|
(:t_aura:`аура`, :t_occult:`оккультный`, :t_necromancy:`некромантия`, :t_death:`смерть`, :t_visual:`визуальный`)
**Триггер**: Свой ход начинает живое существо в пределах 30 футов от бодака, которое он может воспринимать своей жизнечувствительностью;
**Эффект**: Цель должна совершить спасбросок Стойкости КС 23.
При провале, бодак восстанавливает 5 ОЗ и цель становится :c_drained:`истощена 1`.

----------

**Скорость**: 20 футов


**Ближний бой**: |д-1| кулак +18 [+14/+10] (:w_agile:`быстрое`, :w_finesse:`точное`),
**Урон** 2d6+6 дробящий + 1d6 негативный


**Смертельный взгляд (Death Gaze)** |д-2|
(:t_occult:`оккультный`, :t_necromancy:`некромантия`, :t_death:`смерть`, :t_visual:`визуальный`)
Бодак пристально смотрит на живое существо в пределах 30 футов, которое он может ощущать своей жизнечувствительностью.
Это существо должно совершить спасбросок Стойкости КС 26.
Если цель становится :c_drained:`истощенной`, бодак получает количество временных ОЗ, равное полученной величине состояния :c_drained:`истощения` помноженной на 5.
Многократное воздействие этого умения может увеличить состояние :c_drained:`истощения` существа максимум до 4.
Если бодак уничтожен, все состояния :c_doomed:`обреченности`, полученные существом от "Смертельного взгляда", снимаются.

| **Критический успех**: Существо невредимо.
| **Успех**: Существо :c_drained:`истощено 1`.
| **Провал**: Существо :c_doomed:`обречено 1` и :c_drained:`истощено 2`.
| **Критический провал**: Существо :c_doomed:`обречено 1` и :c_drained:`истощено 4`.


**Порождение бодака (Bodak Spawn)**
(:t_occult:`оккультный`, :t_necromancy:`некромантия`)
Любой :t_humanoid:`гуманоид`, умерший находясь под :c_drained:`истощением` или :c_doomed:`обреченностью` от бодака, через 24 часа после смерти восстает как самостоятельный бодак.





.. include:: /helpers/actions.rst
.. include:: /helpers/bestiary-icons.rst