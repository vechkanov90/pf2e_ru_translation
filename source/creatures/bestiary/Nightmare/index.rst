.. include:: /helpers/roles.rst

.. rst-class:: creature-details

***********************************************************************************************************
Кошмар (`Nightmare <https://2e.aonprd.com/MonsterFamilies.aspx?ID=76>`_)
***********************************************************************************************************

**Источник**: Bestiary pg. 244



.. toctree::
   :glob:
   :maxdepth: 3
   
   *



.. rst-class:: h3
.. rubric:: Кошмары-скакуны (Nightmare Steeds)

|creatures|

**Источник**: Bestiary pg. 244

Кошмары позволяют оседлать себя только злейшим существам и охотно участвуют в разрушениях, которые причиняют эти существа.
Особенно хорошо известно, что с кошмарами ассоциируют :doc:`ночных карг </creatures/bestiary/Hag/Night-Hag>`.





.. include:: /helpers/actions.rst
.. include:: /helpers/bestiary-icons.rst