.. include:: /helpers/roles.rst


.. rst-class:: creature-details

Шаукин (Shaukeen)
============================================================================================================

Шаукины, возможно, самые порочные асуры из всех.
Они с особым удовольствием наслаждаются причинением краха тем, кто оказывает им помощь, дает кров, а главное - учит.
Многие учителя сталкивались шаукином, замаскированным под маленького ребенка с выдающимися талантами или любопытством, и брали его под свое крыло, лишь для того, чтобы потом оказаться обманутыми и погубленными.
Ум этих крохотных асуров несоизмерим с их размерами; они задают проницательные вопросы и склоняют свою жертву к решениям, которые приводят их к ереси, способствуя краху порядка и приличий.
Когда мир ополчается против наставника шаукина, этот асура уже пропадает - но не слишком далеко, ибо им нравится затаиться и наблюдать за мучениями и гибелью своих наставников.



.. rst-class:: creature
.. _bestiary--Shaukeen:

Шаукин (`Shaukeen <https://2e.aonprd.com/Monsters.aspx?ID=1069>`_) / Существо 1
------------------------------------------------------------------------------------------------------------

- :alignment:`ПЗ`
- :size:`крошечный`
- асура
- бес

**Источник**: Bestiary 3 pg. 22

**Восприятие**: +8;
:ref:`ночное зрение <cr_ability--Darkvision>`

**Языки**: Всеобщий,
Инфернальный;
:ref:`телепатия <cr_ability--Telepathy>` (касание)

**Навыки**:
Акробатика +7,
Обман +9,
Скрытность +7,
Выступление +7,
Религия +7

**Сил** +0,
**Лвк** +4,
**Тел** +1,
**Инт** +0,
**Мдр** +3,
**Хар** +4

----------

**КБ**: 16;
**Стойкость**: +4,
**Рефлекс**: +9,
**Воля**: +7

**ОЗ**: 22

**Иммунитеты**: :t_curse:`проклятие`

**Слабости**: добро 2

----------

**Скорость**: 25 футов


**Ближний бой**: |д-1| челюсти +9 [+4/-1] (:w_finesse:`точное`),
**Урон** 1d8 колющий + 1 злом и слюна огненного шакала

**Ближний бой**: |д-1| шип +9 [+5/+1] (:w_agile:`быстрое`, :w_finesse:`точное`),
**Урон** 1d8 колющий + 1 злом


**Врожденные сакральные заклинания** КС 17

| **2 ур.**
| :ref:`spell--c--Charm`
| :ref:`spell--s--Spider-Climb`
| :ref:`spell--t--Touch-of-Idiocy`
| **Чары (1 ур.)** :ref:`spell--r--Read-Aura`
| **Постоянные (3 ур.)**
| :ref:`spell--m--Magic-Aura` (только на шаукина и его предметы)
| :ref:`spell--n--Nondetection` (только на себя)


:ref:`cr_ability--Change-Shape` |д-1|
(:t_divine:`сакральный`, :t_transmutation:`трансмутация`, :t_polymorph:`полиморф`, :t_concentrate:`концентрация`)
Шаукин принимает облик :s_small:`маленького` :t_humanoid:`гуманоида`.
Это не изменяет его Скорость или модификаторы атаки и урона Ударов, но может изменить тип урона, наносимого его Ударами (обычно на дробящий).
Обычно асура теряет свои Удары челюстями и шипом, если только гуманоидная форма не обладает клыками или подобной безоружной атакой.
Эта альтернативная форма имеет конкретную постоянную внешность, которую шаукин может изменять выполняя ритуал длительностью в 1 час.


**Слюна огненного шакала (Fire Jackal Saliva)**
(:t_poison:`яд`)

| **Спасбросок**: Стойкость КС 17
| **Макс.продолжительность**: 6 раундов
| **Стадия 1**: 1d4 ядом и :c_clumsy:`неуклюжесть 1` (1 раунд)
| **Стадия 2**: 1d6 ядом и :c_clumsy:`неуклюжесть 2` (1 раунд)





.. include:: /helpers/actions.rst