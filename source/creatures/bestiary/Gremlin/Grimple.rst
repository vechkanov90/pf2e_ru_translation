.. include:: /helpers/roles.rst


.. rst-class:: creature-details

Гримпл (Grimple)
============================================================================================================

.. sidebar:: |creatures|

	.. rubric:: Союзники гримплов (Grimple Allies)

	**Источник**: Bestiary 3 pg. 120

	:doc:`Вексгиты <Vexgit>` и гримплы иногда объединяются, чтобы устраивать разрушения.
	Насекомоподобные вексгиты, сидящие в заброшенных часовых или колокольных башнях, используют раздражительных гримплов, чтобы заманивать жертв в изощренные смертельные ловушки.
	Предоставленные сами себе, гримплы издеваются над :doc:`митфлитами <Mitflit>` или тренируют :doc:`гигантских крыс </creatures/bestiary/Rat/Giant-Rat>` и :doc:`рои пауков </creatures/bestiary/Spider/Spider-Swarm>` выполнять их приказы.


Гримплам, даже больше чем большинству гремлинов, не нравятся атрибуты цивилизации: общие залы гостиниц с их шумными песнями, конные дворы с их лошадиным ржанием, церковные шпили с их колокольным звоном.
Гримплы живут, чтобы портить эти условности, сбрасывая на посетителей вывески таверн, мочась в дождевые бочки и открывая двери конюшен.
Когда ничего не помогает, они буквально отрыгивают свое презрение на прохожих.

Гримплы похожи на гуманоидных, чесоточных опоссумов, с клыками как у кабана, которые помогают им рыться в мусорных кучах в поисках пищи.
Они проворно лазают и парят с карниза на карниз на свободных лоскутах кожи между конечностями.
Опытные охотники на гремлинов знают, что нужно искать чешуйки кожи и шерсть, которые гримплы вычесывают из своей зараженной паразитами шкуры.



.. rst-class:: creature
.. _bestiary--Grimple:

Гримпл (`Grimple <https://2e.aonprd.com/Monsters.aspx?ID=1177>`_) / Существо -1
------------------------------------------------------------------------------------------------------------

- :alignment:`ХЗ`
- :size:`крошечный`
- гремлин
- фея

**Источник**: Bestiary 3 pg. 120

**Восприятие**: +6;
:ref:`сумеречное зрение <cr_ability--Low-Light-Vision>`

**Языки**: Подземный

**Навыки**:
Природа +4,
Обман +2,
Ремесло +5 (+7 ловушки),
Скрытность +5,
Воровство +5

**Сил** +1,
**Лвк** +3,
**Тел** +3,
**Инт** +1,
**Мдр** +2,
**Хар** -2


**Предметы**:
мешок с 5 камнями

----------

**КБ**: 15;
**Стойкость**: +5,
**Рефлекс**: +7,
**Воля**: +4

**ОЗ**: 9

**Слабости**: :ref:`холодное железо <material--Cold-Iron>` 2


**Гремлинские вши (Gremlin Lice)**:
Всякий раз, когда живое существо прикасается к гримплу или гримпл касается его (в том числе и при успешном безоружном Ударе ближнего боя), оно должно совершить успешный спасбросок Рефлекса КС 13, иначе заразится гремлинскими вшами.
Пока существо заражено, его отвлекает зуд и оно :c_stupefied:`одурманено 1`, хотя оно может использовать действие :ref:`action--Interact`, чтобы почесаться в зудящем месте и подавить состояние :c_stupefied:`одурманенности` от вшей на 1d4 раунда.
Заражение заканчивается через 24 часа или пока существо не погрузится в воду, или не подвергнется воздействию :ref:`сильного холода <table--10-13>`, в зависимости от того, что наступит раньше.

----------

**Скорость**: 10 футов,
карабканье 20 футов,
полет 20 футов


**Ближний бой**: |д-1| укус +7 [+3/-1] (:w_agile:`быстрое`, :w_finesse:`точное`),
**Урон** 1d4+1 колющий


**Дальний бой**: |д-1| камень +7 [+3/-1] (:w_agile:`быстрое`, шаг дистанции 20 фт),
**Урон** 1d4+1 дробящий


**Мерзкая рвота (Putrid Vomit)** |д-1|
Гримпл изрыгает 30-футовую линию рвоты.
Каждое существо в этой линии должно совершить успешный спасбросок Стойкости КС 16, иначе получит :c_sickened:`тошноту 1` (:c_sickened:`тошноту 2` при критическом провале).
Гримпл не может снова использовать "Мерзкую рвоту" 1d4 раунда.


**Врожденные природные заклинания** КС 16

| **1 ур.** :ref:`spell--g--Grease`
| **Чары (1 ур.)**
| :ref:`spell--m--Mage-Hand`
| :ref:`spell--p--Prestidigitation`





.. include:: /helpers/actions.rst
.. include:: /helpers/bestiary-icons.rst